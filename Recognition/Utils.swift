//
//  Utils.swift
//  Speech
//
//  Created by Ivan Kh on 15/08/2017.
//  Copyright © 2017 Google. All rights reserved.
//

import JavaScriptCore

extension JSValue {
    
    func invokeMethodAsync(_ thread: Thread, _ method: String!, _ arguments: [Any]? = nil) {
        let args = NSMutableDictionary()
        args[method] = arguments ?? []
        perform(#selector(_invokeMethod(_:)), on: thread, with: args, waitUntilDone: false)
    }

    @objc private func _invokeMethod(_ args: NSDictionary) {
        
        let method = args.allKeys[0] as! String
        let args   = args.allValues[0] as! [Any]
        
        invokeMethod(method, withArguments: args)
    }
}
